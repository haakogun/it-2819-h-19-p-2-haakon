import React from 'react';
import MiddlePart from './components/MiddlePart';
import Header from './components/Header';
import './App.css';

function App() {
  return (
    <div>
      <Header/>
      <MiddlePart/>
    </div>
  );
}

export default App;
