import React from 'react';
import App from '../App';
import renderer from 'react-test-renderer'

describe('Testing App.js',()=>{

    test('Snapshot match test', () => {
      //Creates an instance of the App component and checkes to see if it matches the snapshot.
      const tree = renderer.create(<App />).toJSON();
      expect(tree).toMatchSnapshot();
    })
})