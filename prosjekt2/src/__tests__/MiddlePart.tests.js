import React from 'react';
import MiddlePart from '../components/MiddlePart';
import { create } from 'react-test-renderer'

describe('Testing the index state',()=>{

    test('testing if the index state variable increments when the nextButton is pressed', () => {
        let tree = create(<MiddlePart />)
        let instance = tree.getInstance();
        let currentIndex = instance.state.currentIndex;
        expect(instance.state.currentIndex).toBe(currentIndex);

        // Changing  the state
        instance.goToNextSlide();

        // Cheking to see if currentIndex increments.
        if(instance.state.currentIndex === 3){
            expect(instance.state.currentIndex).toBe(0);
        }
        else{
            expect(instance.state.currentIndex).toBe(currentIndex+1);
        }
    })

    test('testing if the index state variable deincrements when the prevButton is pressed', () => {
        let tree = create(<MiddlePart />)
        let instance = tree.getInstance();
        let currentIndex = instance.state.currentIndex;
        expect(instance.state.currentIndex).toBe(currentIndex);

        // Changing  the state
        instance.goToPrevSlide();

        // Cheking to see if currentIndex deincrements.
        if (instance.state.currentIndex === 0){
            expect(instance.state.currentIndex).toBe(3);
        }
        else{
            expect(instance.state.currentIndex).toBe(currentIndex-1);
        }
    })

    test('testing if the index state variable changes to 0 when the first navigation button is pressed.', () => {
        let tree = create(<MiddlePart />)
        let instance = tree.getInstance();

        // Changing  the state
        instance.goToFirstSlide();

        expect(instance.state.currentIndex).toBe(0);
    })

    test('testing if the index state variable changes to 1 when the second navigation button is pressed.', () => {
        let tree = create(<MiddlePart />)
        let instance = tree.getInstance();

        // Changing  the state
        instance.goToSecondSlide();

        expect(instance.state.currentIndex).toBe(1);
    })

    test('testing if the index state variable changes to 2 when the third navigation button is pressed.', () => {
        let tree = create(<MiddlePart />)
        let instance = tree.getInstance();

        // changing  the state
        instance.goToThirdSlide();

        expect(instance.state.currentIndex).toBe(2);
    })

    test('testing if the index state variable changes to 3 when the fourth navigation button is pressed.', () => {
        let tree = create(<MiddlePart />)
        let instance = tree.getInstance();

        // changing  the state
        instance.goToFourthSlide();

        expect(instance.state.currentIndex).toBe(3);
    })
})