import React from 'react';
import Header from '../components/Header.jsx';
import renderer from 'react-test-renderer'

describe('Testing Header.jsx',()=>{

    //Creates an instance of the App component and checkes to see if it matches the snapshot.
    test('Snapshot match test', () => {
      const tree = renderer.create(<Header />).toJSON();
      expect(tree).toMatchSnapshot();
    })
})