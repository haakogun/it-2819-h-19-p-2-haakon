import React, {Component} from 'react';
/*
Creates the sidebar to the right.
*/
class Sidebar extends Component {

    constructor(props){
        super(props)
        this.saveFav = this.saveFav.bind(this);
    }

    //Updates the counter and the content to match the index for the text, sound and image.
    seeCombinationClickHandler(){
        this.props.changeContent();
        if (typeof(Storage) !== "undefined") {
            sessionStorage.currentSound = this.props.soundIndex
            sessionStorage.currentImage = this.props.imageIndex
            sessionStorage.currentText = this.props.textIndex
        } else {
            // Sorry! No Web Storage support..
        }
    }

    //saves the combination in local storage
    saveFav(){
        //Checks if user has web storage support
        if (typeof(Storage) !== "undefined") {
            localStorage.favIndex = this.props.currentIndex;
            localStorage.favSound = this.props.soundIndex;
            localStorage.favImage = this.props.imageIndex;
            localStorage.favText = this.props.textIndex;
        } 
    }
    
    //http://react.tips/radio-buttons-in-reactjs/
    render() {
        return (
            <div>
                <div className="sidebar">
                    {/* Sounds */}
                    <div className="sidebarCollection">
                        <h4>Sounds</h4>
                        <form>
                            <label>
                                <input type="radio" value="animals"
                                            checked={this.props.soundIndex === 'animals'} 
                                            onChange={this.props.handleOptionChangeSound} />
                                Animals
                            </label>
                            <div className="radio">
                            <label>
                                <input type="radio" value="laugh" 
                                            checked={this.props.soundIndex === 'laugh'} 
                                            onChange={this.props.handleOptionChangeSound} />
                                Laugh
                            </label>
                            </div>
                            <div className="radio">
                            <label>
                                <input type="radio" value="elevator" 
                                            checked={this.props.soundIndex === 'elevator'} 
                                            onChange={this.props.handleOptionChangeSound} />
                                Elevator music
                            </label>
                            </div>
                        </form>
                    </div>

                    {/* Images */}
                    <div className="sidebarCollection">
                        <h4>Images</h4>
                        <form>
                            <div className="radio">
                            <label>
                                <input type="radio" value="pokemon" 
                                            checked={this.props.imageIndex === 'pokemon'} 
                                            onChange={this.props.handleOptionChangeImage} />
                                Pokemon
                            </label>
                            </div>
                            <div className="radio">
                            <label>
                                <input type="radio" value="dragon" 
                                            checked={this.props.imageIndex === 'dragon'} 
                                            onChange={this.props.handleOptionChangeImage} />
                                Dragon
                            </label>
                            </div>
                            <div className="radio">
                            <label>
                                <input type="radio" value="cat" 
                                            checked={this.props.imageIndex === 'cat'} 
                                            onChange={this.props.handleOptionChangeImage} />
                                Cat
                            </label>
                            </div>
                        </form>
                    </div>

                    {/* Text */}
                    <div className="sidebarCollection">
                        <h4>Texts</h4>
                        <form>
                            <div className="radio">
                            <label>
                                <input type="radio" value="clicheQuotes" 
                                            checked={this.props.textIndex === 'clicheQuotes'} 
                                            onChange={this.props.handleOptionChangeText} />
                                Inspo
                            </label>
                            </div>
                            <div className="radio">
                            <label>
                                <input type="radio" value="mediaQuotes" 
                                            checked={this.props.textIndex === 'mediaQuotes'} 
                                            onChange={this.props.handleOptionChangeText} />
                                Movie quotes
                            </label>
                            </div>
                            <div className="radio">
                            <label>
                                <input type="radio" value="limericks" 
                                            checked={this.props.textIndex === 'limericks'} 
                                            onChange={this.props.handleOptionChangeText} />
                                Limericks
                            </label>
                            </div>
                        </form>
                    </div>
                </div>

                {/* Buttons */}
                <div className="sidebarButtons">
                    <button onClick={(e) => this.seeCombinationClickHandler(e)}>
                        See combination
                    </button>
                    <button onClick={this.saveFav}>
                        Save as favorite
                    </button>
                    <button onClick={this.props.handleGetFav}>
                        Get favorite
                    </button>
                </div>
            </div>
        );
    }
}

export default Sidebar;