import React from 'react';

/*
Creates a component consisting of a <meta> viewport element. 
This gives the browser instructions on how to control the page's dimensions and scaling.
(w3schools.com; https://www.w3schools.com/css/css_rwd_viewport.asp)
*/
function Viewport() {
    return (
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    )
}

export default Viewport;