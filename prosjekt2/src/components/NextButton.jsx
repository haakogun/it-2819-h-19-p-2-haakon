import React from 'react';
/*
Creates the next button to the right.
*/
class NextButton extends React.Component{

  render(){
    return (
      <div className="slideButtonContainer">
          <button className='nextBtn' onClick={this.props.goToNextSlide}>&#10095;</button>
      </div>
    );
  }

}

export default NextButton;


