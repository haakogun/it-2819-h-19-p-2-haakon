import React from 'react';
import ImageBox from "./ImageBox";
import TextBox from './TextBox';
import SoundBox from "./SoundBox";
import NextButton from './NextButton';
import PrevButton from './PrevButton';

/* 
Creates a component with layout of svg-image, text and audio for smaller screens.
*/
class SmallScreenBox extends React.Component{
  /*
  Updates the content in SoundBox, ImageBox and TextBox.
  */
  changeContentPB = () => {
    this.refs.SoundBox.changeSoundContentSB();
    this.refs.ImageBox.getSVG();
    this.refs.TextBox.getText();
  }

  render(){
    let currentIndex = this.props.currentIndex;
    let imageIndex = this.props.imageIndex;
    let soundIndex = this.props.soundIndex;
    let textIndex = this.props.textIndex;

    return (
      <div className="container">
        <PrevButton goToPrevSlide={this.props.goToPrevSlide}/>
        <div className="col">
          <ImageBox ref="ImageBox" currentIndex={currentIndex} imageIndex={imageIndex} />
          <TextBox ref="TextBox" currentIndex={currentIndex} textIndex={textIndex} />
          <SoundBox ref="SoundBox" currentIndex={currentIndex} soundIndex={soundIndex} />
        </div>
        <NextButton goToNextSlide={this.props.goToNextSlide}/>
      </div>
    );
  }
}

  export default SmallScreenBox;