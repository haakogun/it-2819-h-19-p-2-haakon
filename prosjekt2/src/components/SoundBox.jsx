import React from 'react';
import cat from '../assets/sounds/Cat.mp3';
import horse from '../assets/sounds/Horse.mp3';
import donkey from '../assets/sounds/donkey.mp3';
import wolf from '../assets/sounds/Wolf.mp3';
import cartoonLaugh from '../assets/sounds/CartoonLaugh.mp3'
import crowdLaugh from '../assets/sounds/crowdLaugh.mp3'
import laughTrack from '../assets/sounds/laughTrack.mp3'
import tidusLaugh from '../assets/sounds/TidusLaugh.mp3'
import elevator1 from '../assets/sounds/elevatorMusic1.mp3'
import elevator2 from '../assets/sounds/elevatorMusic2.mp3'
import elevator3 from '../assets/sounds/elevatorMusic3.mp3'
import elevator4 from '../assets/sounds/loungeAmbient.mp3'
/*
Creates the sound box.
*/
class SoundBox extends React.Component{
  constructor(props){
    super(props);

    //Loads the sound files directly
    this.state = {
      animals: [cat,horse,donkey,wolf],
      laugh: [tidusLaugh,cartoonLaugh,crowdLaugh,laughTrack],
      elevator: [elevator1,elevator2,elevator3,elevator4],
      sounds: [cat,horse,donkey,wolf]
    }

  }
  changeSoundContentSB = () => {
    if (this.props.soundIndex === 'animals'){
      this.setState({sounds: this.state.animals});
    }
    if (this.props.soundIndex === 'laugh'){
      this.setState({sounds: this.state.laugh});
    }
    if (this.props.soundIndex === 'elevator'){
      this.setState({sounds: this.state.elevator});
    }
    this.render();
  }

  componentDidMount(){
    this.changeSoundContentSB();
  }

  render(){
    let currentIndex = this.props.currentIndex;

    return (
      <div style={{display: 'flex'}}>
        <div className="box sound">
          <audio ref="audio_tag" src={this.state.sounds[currentIndex]} type="audio/mpeg" controls/>
        </div>
      </div>
    );
  }
}

export default SoundBox;