import React from 'react';
/*
Creates the text box.
*/
class TextBox extends React.Component{

  constructor(props){
    super(props);

    this.getText = this.getText.bind(this);

    /* 
    The boolean variable keeps track of whether or not the component is mounted.
    Is used to prevents the attempt of setting the state of a component that has unmounted, 
    which is a danger because fetch is asynchronous.
    */
    this._isMounted = false;

    this.state = {
      text: '',
      author: ''
    }
  }

  /* 
  Fetches the correct json-file, and displays different texts and authors depending on currentIndex 
  only if component is mounted.
  */
  getText() {
    fetch('assets/text/'+this.props.textIndex+'.json', {
          headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
      }})
      .then((response) => response.json())
      .then((myJson) => {
        if (this._isMounted){
          let obj = myJson[this.props.currentIndex];
          this.setState({
            text: obj["text"],
            author: obj["author"]
          });
          return myJson;
        }
      })
      .catch(err => console.err)
  };

  componentDidMount(){
    this._isMounted = true;
    this.getText();
  }

  componentWillUnmount(){
    this._isMounted = false;
  }
  
  render(){
    return (
      <div id="text-container" className="box text">
        <q>{this.state.text}</q>
        <p>-{this.state.author}</p>
      </div>
    );
  }
}

export default TextBox;