import React from 'react';
import SmallScreenBox from './SmallScreenBox';
import LargeScreenBox from './LargeScreenBox';
import Viewport from './Viewport';
import NavigationDots from './NavigationDots';
import Sidebar from './Sidebar';
import Footer from './Footer';
import SmallLandscapeBox from './SmallLandscapeBox';

class MiddlePart extends React.Component{
  
  constructor(props){
    super(props);

    this.handleOptionChangeSound = this.handleOptionChangeSound.bind(this);
    this.handleOptionChangeImage = this.handleOptionChangeImage.bind(this);
    this.handleOptionChangeText = this.handleOptionChangeText.bind(this);
    this.handleGetFav = this.handleGetFav.bind(this);
    this.changeContent = this.changeContent.bind(this);
    //State keeps track of which category of text, sound and image is selected
    //Check if the user has stored anything, and if so then use it
    if (sessionStorage.currentText && sessionStorage.currentIndex){
      //User has stored both radio button combination and navigation index
      this.state = {
        currentIndex: Number(sessionStorage.currentIndex),
        soundIndex: sessionStorage.currentSound,
        imageIndex: sessionStorage.currentImage,
        textIndex: sessionStorage.currentText
      }
    }
    else if (sessionStorage.currentIndex){
      //Only stored nagivation index
      this.state = {
        currentIndex: Number(sessionStorage.currentIndex),
        soundIndex: 'animals',
        imageIndex: 'pokemon',
        textIndex: 'clicheQuotes'
      }
    }
    else{
      //Nothing stored, first time on the website in that session. Use default values
      this.state = {
        currentIndex: 0,
        soundIndex: 'animals',
        imageIndex: 'pokemon',
        textIndex: 'clicheQuotes'
      }
    }
    
    // Boolean variables to know when content should be changed, 
    // and when favorite collection should be displayed.
    this._updateIndex = false;
    this._fetchFav = false;
  }

  //--------------------------------------------------------------------------------------------------------------------------------------

  //Radio button combination stuff

  //--------------------------------------------------------------------------------------------------------------------------------------

  handleOptionChangeSound(changeEvent) {this.setState({soundIndex: changeEvent.target.value});}
  handleOptionChangeImage(changeEvent) {this.setState({imageIndex: changeEvent.target.value});}
  handleOptionChangeText(changeEvent) {this.setState({textIndex: changeEvent.target.value});}

  changeContent = () => {
    let width = window.innerWidth;
    let height = window.innerHeight;

    if (width < 768 && (width > height)) {
      this.refs.SmallLandscapeBox.changeContentPB();
      this.refs.Footer.updateNavDots();
    }
    else if (width < 768 || (width < height)) {
      this.refs.SmallScreenBox.changeContentPB();
      this.refs.Footer.updateNavDots();
    }
    else{
      this.refs.LargeScreenBox.changeContentPB();
      this.refs.NavigationDots.toggleClasses();
    }
  }
  
  //Set the radio buttons to favorite combination with HTML Storage
  handleGetFav(){
    if(localStorage.favSound){
      this.setState({
        currentIndex: Number(localStorage.favIndex),
        soundIndex: localStorage.favSound,
        imageIndex: localStorage.favImage,
        textIndex: localStorage.favText
      });
      sessionStorage.currentIndex = localStorage.favIndex
      sessionStorage.currentSound = localStorage.favSound
      sessionStorage.currentImage = localStorage.favImage
      sessionStorage.currentText = localStorage.favText
      this._fetchFav = true;
    }
  }
  //--------------------------------------------------------------------------------------------------------------------------------------
  
  //Slide combination stuff

  //--------------------------------------------------------------------------------------------------------------------------------------
  //Function for the prev button
  goToPrevSlide = () => {
    if(this.state.currentIndex === 0){
      this.setState({currentIndex: 3})
      sessionStorage.currentIndex = 3;
    }
    else{
      this.setState({currentIndex: this.state.currentIndex - 1});
      sessionStorage.currentIndex = this.state.currentIndex -1;
    }
    this._updateIndex = true;
  }

  //Function for the next button
  goToNextSlide = () => {
    if(this.state.currentIndex === 3){
      this.setState({currentIndex: 0})
      sessionStorage.currentIndex = 0;
    }
    else{
      this.setState({currentIndex: this.state.currentIndex + 1});
      sessionStorage.currentIndex = this.state.currentIndex +1;
    }
    this._updateIndex = true;
  }

  //Functions for navigation buttons at the bottom of the page.
  goToFirstSlide = () => {
    this.setState({currentIndex: 0})
    sessionStorage.currentIndex = 0;
    this._updateIndex = true
  }
  goToSecondSlide = () => {
    this.setState({currentIndex: 1})
    sessionStorage.currentIndex = 1;
    this._updateIndex = true
  }
  goToThirdSlide = () => {
    this.setState({currentIndex: 2})
    sessionStorage.currentIndex = 2;
    this._updateIndex = true
  }
  goToFourthSlide = () => {
    this.setState({currentIndex: 3})
    sessionStorage.currentIndex = 3;
    this._updateIndex = true
  }

  /* 
  When called, the resize-function rerenders the MiddlePart-component.
  */
  resize = () => this.forceUpdate();

  /* 
  Adds eventlistener for when window on screen is resized.
  */
  componentDidMount(){
    window.addEventListener("resize", this.resize);
  }

  
  /* 
  Updates displayed content only when a change is made.
  */
  componentDidUpdate(){
    if (this._updateIndex){
      this.changeContent()
    }
    if(this._fetchFav){
      this.changeContent();
    }
    this._updateIndex = false;
    this._fetchFav = false;
  }

  /* 
  Removes eventlistener for when window on screen is resized.
  */
  componentWillUnmount() {
    window.removeEventListener("resize", this.resize);
  }
  //--------------------------------------------------------------------------------------------------------------------------------------
  
  render(){
      let width = window.innerWidth;
      let height = window.innerHeight;
      /*
      Either shows layout for smaller or larger screens based on width of window on screen and orientation. 
      */
      if (width < 768 && (width > height)) {
        return (
          <div>
            <Sidebar soundIndex={this.state.soundIndex}
              imageIndex={this.state.imageIndex}
              textIndex={this.state.textIndex}
              handleOptionChangeSound={this.handleOptionChangeSound}
              handleOptionChangeImage={this.handleOptionChangeImage}
              handleOptionChangeText={this.handleOptionChangeText}
              getSessionCheckedRadioButtons={this.getSessionCheckedRadioButtons}
              handleGetFav={this.handleGetFav}
              changeContent={this.changeContent}/>
            <div className="container main">
              <Viewport/>
              <SmallLandscapeBox ref="SmallLandscapeBox"
                currentIndex={this.state.currentIndex} 
                imageIndex={this.state.imageIndex}
                soundIndex={this.state.soundIndex}
                textIndex={this.state.textIndex}
                goToPrevSlide={this.goToPrevSlide}
                goToNextSlide={this.goToNextSlide}/>
            </div>
            <Footer ref="Footer"
              currentIndex={this.state.currentIndex}
              goToFirstSlide={this.goToFirstSlide}  
              goToSecondSlide={this.goToSecondSlide}  
              goToThirdSlide={this.goToThirdSlide}  
              goToFourthSlide={this.goToFourthSlide}/>
          </div>
        );
      }
      else if (width < 768 || (width < height)) {
        return (
          <div>
            <Sidebar
              currentIndex={this.state.currentIndex}
              soundIndex={this.state.soundIndex}
              imageIndex={this.state.imageIndex}
              textIndex={this.state.textIndex}
              handleOptionChangeSound={this.handleOptionChangeSound}
              handleOptionChangeImage={this.handleOptionChangeImage}
              handleOptionChangeText={this.handleOptionChangeText}
              getSessionCheckedRadioButtons={this.getSessionCheckedRadioButtons}
              handleGetFav={this.handleGetFav}
              changeContent={this.changeContent}/>
            <div className="container main">
              <Viewport/>
              <SmallScreenBox ref="SmallScreenBox"
                currentIndex={this.state.currentIndex} 
                imageIndex={this.state.imageIndex}
                soundIndex={this.state.soundIndex}
                textIndex={this.state.textIndex}
                handleOptionChangeSound={this.handleOptionChangeSound}
                handleOptionChangeImage={this.handleOptionChangeImage}
                handleOptionChangeText={this.handleOptionChangeText}
                getSessionCheckedRadioButtons={this.getSessionCheckedRadioButtons}
                handleGetFav={this.handleGetFav}
                changeContent={this.changeContent}
                goToPrevSlide={this.goToPrevSlide}
                goToNextSlide={this.goToNextSlide}/>
            </div>
            <Footer ref="Footer"
              currentIndex={this.state.currentIndex}
              goToFirstSlide={this.goToFirstSlide}  
              goToSecondSlide={this.goToSecondSlide}  
              goToThirdSlide={this.goToThirdSlide}  
              goToFourthSlide={this.goToFourthSlide}/>
          </div>
        );
      }
      else{
        return (
          <div className="screenBox">
            <div className="container col main">
              <div className="container">
                <Viewport/>
                <LargeScreenBox ref="LargeScreenBox" 
                  currentIndex={this.state.currentIndex}
                  imageIndex={this.state.imageIndex}
                  soundIndex={this.state.soundIndex}
                  textIndex={this.state.textIndex}
                  goToPrevSlide={this.goToPrevSlide}
                  goToNextSlide={this.goToNextSlide} />
              </div>
              <NavigationDots ref="NavigationDots"
                currentIndex={this.state.currentIndex}
                goToFirstSlide={this.goToFirstSlide}  
                goToSecondSlide={this.goToSecondSlide}  
                goToThirdSlide={this.goToThirdSlide}  
                goToFourthSlide={this.goToFourthSlide} />
            </div>
            <Sidebar 
              currentIndex={this.state.currentIndex}
              soundIndex={this.state.soundIndex}
              imageIndex={this.state.imageIndex}
              textIndex={this.state.textIndex}
              handleOptionChangeSound={this.handleOptionChangeSound}
              handleOptionChangeImage={this.handleOptionChangeImage}
              handleOptionChangeText={this.handleOptionChangeText}
              getSessionCheckedRadioButtons={this.getSessionCheckedRadioButtons}
              handleGetFav={this.handleGetFav}
              changeContent={this.changeContent}/>
          </div>
        );
      };
  }
}

export default MiddlePart;