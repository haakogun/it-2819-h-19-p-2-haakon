import React from 'react';

/* 
Creates a div for the svg-images.
*/
class ImageBox extends React.Component{

  constructor(props){
    super(props);

    this.getSVG = this.getSVG.bind(this);
    this.getSVGnames = this.getSVGnames.bind(this);

    /* 
    The boolean variable keeps track of whether or not the component is mounted.
    Is used to prevents the attempt of injecting a svg-image in a div that does not exist any more, 
    which is a danger because fetch is asynchronous.
    */
    this._isMounted = false;
  }

  /* 
  Finds the correct name for the svg-image to be displayed.
  */
  getSVGnames(option){
    var svgFiles = {'cat': ['chillax', 'fluff', 'lick', 'stare'], 
                    'dragon': ['b&w', 'cartoon', 'flying', 'two'],
                    'pokemon': ['instinct', 'magikarp', 'mystic_2', 'valore'] 
    };
      return svgFiles[option];
  };

  /* 
  Fetches the svg-image to be displayed, and displays it in the div with id="svg-container" after emptying it first.
  */
  getSVG() {
    let svg_name = this.getSVGnames(this.props.imageIndex)[this.props.currentIndex];
    let con = document.getElementById("svg-container");
    if (con !== null){
      while (con.children.length > 0){
        con.removeChild(con.lastElementChild);
      }
    }
    fetch('assets/svg-files/'+this.props.imageIndex+'_'+svg_name+'.svg')
        .then(response => response.text())
        .then(svg => {
          if (this._isMounted){
            document.getElementById("svg-container").insertAdjacentHTML("afterbegin", svg)
          }
        })
  };

  componentDidMount(){
    this._isMounted = true;
    this.getSVG();
  }

  componentWillUnmount(){
    this._isMounted = false;
  }

  render(){
    return (
      <div id="svg-container" className="box image">
        
      </div>
    );
  }
}

export default ImageBox;