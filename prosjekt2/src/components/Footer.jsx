import React, {Component} from 'react';
import NavigationDots from './NavigationDots';

/* 
Creates a footer with navigation-dots. Is used for small screens.
*/
class Footer extends Component {
  /*
  Calls the function toggleClasses in NavigationDots to update the dot-buttons.
  */
  updateNavDots(){
    this.refs.NavigationDots.toggleClasses();
  }

  render() {
    return (
      <div className="footer" id="footer">
        <NavigationDots ref="NavigationDots"
          currentIndex={this.props.currentIndex}
          goToFirstSlide={this.props.goToFirstSlide}  
          goToSecondSlide={this.props.goToSecondSlide}  
          goToThirdSlide={this.props.goToThirdSlide}  
          goToFourthSlide={this.props.goToFourthSlide} />
      </div>
    )
  }
}

export default Footer;