import React from 'react';

/*
Creates a header with just the title of the project.
*/
function Header(){
  return (
    <div className="header" id="header">
          Awesome art collections!
    </div>
  )
}

export default Header;
