import React from 'react';

/*
Creates round buttons for navigating between the different compositions in a combination.
*/
class NavigationDots extends React.Component{
  constructor(props){
    super(props)
    this.toggleClasses = this.toggleClasses.bind(this)
  }

  /*
  Updates the classes of the dot-buttons.
  */
  toggleClasses = () => {
    if (document.getElementById('dot_0') === null){
      return
    }
    for (let i=0; i<4; i++){
      if (this.props.currentIndex===i){
        document.getElementById('dot_'+i).classList += [" activeDot"]
      }
      else{
        document.getElementById('dot_'+i).classList = ["dot"]
      }
    }
  }

  componentDidMount(){
    this.toggleClasses()
  }

  render(){
    return (
      <div className="navDots">
          <button id="dot_0" className = "dot" onClick={this.props.goToFirstSlide}></button> 
          <button id="dot_1" className = "dot" onClick={this.props.goToSecondSlide}></button> 
          <button id="dot_2" className = "dot" onClick={this.props.goToThirdSlide}></button>
          <button id="dot_3" className = "dot" onClick={this.props.goToFourthSlide}></button> 
      </div>
    );
  }
}

export default NavigationDots;


