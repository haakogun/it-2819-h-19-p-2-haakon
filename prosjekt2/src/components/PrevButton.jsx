import React from 'react';
/*
Creates the prev button to the left.
*/
class PrevButton extends React.Component{
    render(){
      return (
        <div className="slideButtonContainer">
            <button className='prevBtn' onClick={this.props.goToPrevSlide}>&#10094;</button>
        </div>
      );
    }

}

export default PrevButton;


