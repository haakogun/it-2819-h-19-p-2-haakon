import React from 'react';
import ImageBox from "./ImageBox";
import TextBox from './TextBox';
import SoundBox from "./SoundBox";
import NextButton from './NextButton';
import PrevButton from './PrevButton';

/* 
Creates a component with layout of svg-image, text and audio for larger screens.
*/
class LargeScreenBox extends React.Component{
  /*
  Updates the content in SoundBox, ImageBox and TextBox.
  */
  changeContentPB = () => {
    this.refs.SoundBox.changeSoundContentSB();
    this.refs.ImageBox.getSVG();
    this.refs.TextBox.getText();
  }

  render(){
    let currentIndex = this.props.currentIndex;
    let imageIndex = this.props.imageIndex;
    let soundIndex = this.props.soundIndex;
    let textIndex = this.props.textIndex;


    return (
      <div style={{display: 'flex'}}>
        <PrevButton goToPrevSlide={this.props.goToPrevSlide}/>
        <ImageBox ref="ImageBox" currentIndex={currentIndex} imageIndex={imageIndex}/>
        <div className="container col">
          <TextBox ref="TextBox" currentIndex={currentIndex} textIndex={textIndex}/>
          <SoundBox ref="SoundBox" currentIndex={currentIndex} soundIndex={soundIndex}/>
        </div>
        <NextButton goToNextSlide={this.props.goToNextSlide}/>
      </div>
    );
  }
}

export default LargeScreenBox;