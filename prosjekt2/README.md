## Dokumentasjon
 
### `React`
I prosjektet er det blitt brukt React og JSX, for å lage produktet. Gruppen lagde prosjektet ved å benytte Node sin innebygde *create-react-app*. Dette satte opp ulike mapper og filer som er nødvendig for en react web-applikasjon, for eksempel src-mappa, App.js-fil, etc. Siden gruppen ikke hadde noen tidligere erfaring med React, ble resten av filstrukturen satt opp slik som ble anbefalt etter mangfoldige søk på web. Det vil si at man har en egen components-mappe med jsx-komponenter og assets-mappe med mediafiler som svg-bildefiler, tekstfiler i json og lydfiler i mp3. Gjennom prosjektet har *npm start* blitt brukt for å lage *development build* av prosjektet. Videre har gruppen lært og utnyttet React sine funksjonaliteter som state, props og refs. I dette prosjektet er komponentene skrevet hovedsakelig med bruk av klasser i ES6, men noen er laget som funksjoner for å kunne lære hvordan man kan gjøre det samme på ulike måter. Med andre ord har prosjektet blitt laget med utgangspunkt i det som gruppen fant som anbefalt, og brukt React-funksjonaliteter flittig.
 
Underveis i prosjektet har gruppen tilegnet seg kunnskap om de ordinære mekanismene i React som å lagre og endre state/data, og å implementere alle UI-elementer fra bunnen av. 
 
### `Ajax`
SVG-filene og json-filene som er brukt lastes inn via ajax sin *fetch*-metode. Dette gjør at filene kun lastes inn dersom det er nødvendig. I motsetning, gjøres henting av lyd kun med vanlig importering. 
 
Gruppen har valgt å ikke implementere noen egen form for caching, ettersom dette allerede eksisterer i moderne nettlesere. Dette er vist med bildene caching1 og caching2, som ligger i prosjekt2 mappa. I caching1 er caching disabled, dette vises med at status code er 200 OK selv om filen har vært lastet inn før. Det vil si at bildet har blitt hentet på nytt, og at dragon-bildet ikke var lagret i cachen. Caching2 viser at når man har caching enabled, så får man status code 304 Not Modified om filen har vært hentet før og ikke blitt modifisert siden. Dette betyr at man ikke trengte å hente dragon-bildet på nytt, ettersom det allerede var lagret i cachen og ikke hadde blitt modifisert. 
 
### `Opphavsrett`
For alle tekster som er brukt, har gruppen inkludert sitattegn og opphavsperson. Bildene er fra Pixabay, som tilbyr en stor samling gratis arkivbilder, vektorer og kunstillustrasjoner. Alle bilder er utgitt under Creative Commons CC0. Lydklippene brukt er hentet fra Freesound eller spilt inn selv. 
 
### `HTML Web Storage`
 
I prosjektet er det brukt både *local storage* og *session storage*. *Local storage* har gruppen valgt å utnytte ved å gi brukeren mulighet til å lagre sin favorittkombinasjon av bilde, lyd og tekst. *Session storage* brukes til at dersom man laster inn nettsiden på nytt, vil man komme tilbake på nettsiden med samme kombinasjon som var før re-innlastningen.
 
### `Responsive Web Design`
Viewport ble satt ved å legge inn en <meta>-tag med *name=”viewport”*. Inne i <meta>-taggen ble *width*, dvs. bredden til skjermen, satt til å følge bredden til vinduet vist på skjermen. I tillegg ble *initial-scale*, dvs. den initielle zoomen når nettsiden lastes, satt til å være 1.0.
 
Media-queries er flittig brukt i *App.css*, for å endre på utseende avhengig av hvilken størrelse det er på skjermen og skjermens orientasjon. Her har gruppen valgt å ta utgangspunkt i flere ulike størrelser, med intervaller mellom: større enn 1200px, 1200px, 992px, 768px, 600px, 410px og mindre enn 410px. Disse er henholdsvis: ekstra store enheter (store datamaskinskjermer), store enheter (datamaskiner), middels enheter (nettbrett), små enheter (mobile nettbrett og store mobiltelefoner), ekstra små enheter (mobiltelefoner), og enda mindre enheter (små mobiltelefoner). Dermed har hovedgruppene av enheter blitt dekket. Gruppen har i tillegg tatt hensyn til om enheten er i portrettmodus eller landskapsmodus.
 
Bildet, teksten, lydavspilleren og interaksjonskomponentene som er vist på nettsiden skalerer til størrelsen på skjermen. Dette gjøres ved å sette flere av css-attributtene med *vh* og *vw*, blant annet *height* og *weight*.
 
For å skape en flytende og fleksibel layout har det blitt benyttet CSS-Flexbox. Det ble diskutert om CSS-Grid heller skulle brukes, men siden flere av gruppemedlemmene hadde brukt CSS-grid i prosjekt 1 og ønsket å lære seg Flexbox også, ble Flexbox valgt. Siden nettsiden benytter både rader og kolonner, kunne CSS-grid ha blitt brukt istedenfor, men Flexbox fungerte fint for dette formålet.
 
Det er ikke brukt noen eksterne CSS-rammeverk.
 
### `Testing`
Det er mange ulike måter å kjøre tester på, men gruppen har bestemt seg for å ha testene sine i en *__tests__* mappe. Der har gruppa valgt ut noen få komponenter som det er skrevet enhetstester til. Ellers ble det skrevet en snapshot-test for å teste om websiden ble rendret riktig, samt en mer detaljert snapshot-test av enkeltkomponenten *Header*.
Gruppen har også testet nettsiden manuelt med development build på PC, mobil og andre enheter gjennom nettleser-verktøyet *toogle device toolbar*.
 
### `Virtuell Maskin`
Det ble laget en production build av prosjektet ved å bruke *npm run build*. De produserte filene ble plassert i en build-mappe, viss innhold ble kopiert til en virtuell maskin. For å få til dette ble apache-server installert, og ubuntu 16 brukt til å kopiere og flytte filene til den aktuelle URL-en.
 
### `Bruk av git og koding`
Git har blitt hyppig brukt gjennom hele prosjektprosessen. Etter *create-react-app* ble kjørt, ble prosjektet pushet til gitlab. Deretter forsikret gruppen seg om  at alt av git-funksjonalitet fungerte hos alle gruppemedlemmene. Alle i gruppen bruker Visual Studio Code (VS Code) som tekst-editor. VS Code har innebygd kompatibilitet med git. Dermed ble git brukt hos alle, og da gruppen opplevde merge conflics, kunne det lett løses i VS Code.
 
Hver uke har det blitt laget issues som har blitt utdelt til et medlem og satt frist til neste møte, neste uke eller senere. Den første uken av prosjektet prøvde gruppen å ha en branch per medlem. Dette fungerte greit, men neste uke ønsket gruppen å heller ha brancher basert på hvilken funksjonalitet som ble utviklet. Derfor ble det laget en branch til hvert issue. I noen tilfeller ble tilhørende issues samlet til en overordnet branch. Gruppen kunne vurdert å da slå sammen issuesene som ble tilhørende samme branch, men allikevel ønsket gruppemedlemmene å ha relativt detaljerte issues og valgte å ikke slå issuesene sammen. Etter å ha prøvd begge disse fremgangsmåtene for branch-struktur, har gruppen foretrukket å ha en branch per issue/overordnede issue. Det kan komme av at det da blir mer oversiktlig, og at andre kan jobbe på samme issue lettere om de ønsker. På den andre siden, kan det bli litt rotete med den store mengden brancher, eller om innholdet som blir endret i en branch også dekker innholdet som endres i en annen branch. Dermed har ikke gruppen helt fastslått hva slags branch-struktur som vil benyttes i fremtidige prosjekter.
 
I løpet av prosjektet har gruppen ikke tatt utgangspunkt i *mobile first*. Dette er fordi gruppen hadde mer tidligere erfaring med nettdesign på datamaskin, og følte det var hensiktsmessig å ta utgangspunkt i noe kjent. Gruppen støtte ikke på noen problemer ved å gjøre dette.

### `Kilder`
3 Try REACTJS Tutorial - Display Json Data. (2018). Hentet fra:
    https://www.youtube.com/watch?v=9C85o8jIgUU&fbclid=IwAR08HyNZ-VLLTn50-NO7ogQB21CqWJmxRKlWQqhE3rtPjgMDUsQrbCDNOQU

Berebecki, P. (udatert). React JS Radio buttons. Hentet fra CodePen website: 
    https://codepen.io/PiotrBerebecki/details/mAxPvj

Bowen, B. (udatert). Load SVG Using Fetch API. Hentet fra CodePen website: 
    https://codepen.io/osublake/details/OMRdKq

Cavalcante, R. P. G. (2018, mai 4). Webpack and Dynamic Imports: Doing it Right. Hentet fra Medium website: 
    https://medium.com/front-end-weekly/webpack-and-dynamic-imports-doing-it-right-72549ff49234

Cronin, M. (2018, juni 12). That data looks so fetching on you: Understanding the JS Fetch API. Hentet fra Medium website: 
    https://itnext.io/that-data-looks-so-fetching-on-you-understanding-the-js-fetch-api-880eae0c8d25

Element.insertAdjacentHTML(). (2019, mars 23). Hentet fra MDN Web Docs website: 
    https://developer.mozilla.org/en-US/docs/Web/API/Element/insertAdjacentHTML

Fedosejev, A. (udatert). Radio Buttons In React.js - React Tips. Hentet fra 
    http://react.tips/radio-buttons-in-reactjs/

Fullstack React: Introduction to Promises. (2016, oktober 19). Hentet fra Fullstack React website: 
    https://www.fullstackreact.com/30-days-of-react/day-15/?fbclid=IwAR3LEuy6bI6J1EkhfSpDb8VOCA-KmsJd3fmX6405jmlhAHXoBhccAeDUsDQ

Gowtham, S. (2019, februar 1). React Testing tutorial for beginners using jest. Hentet fra Reactgo website: 
    https://reactgo.com/react-testing-tutorial-jest/

HTML5 Web Storage. (udatert). Hentet fra
    https://www.w3schools.com/html/html5_webstorage.asp

javascript - Cancel All Subscriptions and Asyncs in the componentWillUnmount Method, how? (2018, august 28). Hentet fra Stack Overflow website: 
    https://stackoverflow.com/questions/52061476/cancel-all-subscriptions-and-asyncs-in-the-componentwillunmount-method-how?fbclid=IwAR0nyMKDJHRvYrGiH1-TUEhRHr5G8cuSgpsaZpLt4wXhz8p5FYlpzHUZeEw

javascript - Fetch -API returning HTML instead of JSON. (2016, november 15). Hentet fra Stack Overflow website: 
    https://stackoverflow.com/questions/40602751/fetch-api-returning-html-instead-of-json?fbclid=IwAR2yR9LlcLHrtk9kn674l99i-yYekMarOFfWT0HGna1NcGj22-aevk26q_c

Kingsley, S. (2018, august 16). Working with refs in React. Hentet fra CSS-Tricks website: 
    https://css-tricks.com/working-with-refs-in-react/

React Tutorial - Learn React - React Crash Course [2019]. (2018). Hentet fra 
    https://www.youtube.com/watch?v=Ke90Tje7VS0

Singh, P. (2019, april 16). Remove all the child elements of a DOM node in JavaScript. Hentet  fra GeeksforGeeks website: 
    https://www.geeksforgeeks.org/remove-all-the-child-elements-of-a-dom-node-in-javascript/

Window.localStorage. (2019, august 14). Hentet fra MDN Web Docs website: 
    https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage

Zuzevich, D. (2017, januar 29). Build An Image Slider With React & ES6. Hentet  fra Medium website: 
    https://medium.com/@ItsMeDannyZ/build-an-image-slider-with-react-es6-264368de68e4
 